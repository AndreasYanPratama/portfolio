// content to-content
let nama = document.getElementById("nama");
let n = [
    {nama: "Andreas Yan Pratama", role:["Programmer","Full Stack Developer"]}
];
$.each(n, function(i){
    let roles="";
    for(j=0; j<n[i].role.length; j++){
        roles += "I'm "+n[i].role[j]+";";
    };
    let templateString = 
    `
    <h1 class="wow fadeIn"><strong>`+n[i].nama+`</strong></h1>
    <div class="description wow fadeInLeft">
    <p class="type-text teletype" title="Hi Everybody !;Welcome To My Portfolio;"><span class="teletype-text">`+roles+`</span><span class="teletype-cursor" style="opacity: 0.924255107490752;"></span></p>
    </div>
    `;
    nama.innerHTML += templateString;
})

// content skills
let skills = document.getElementById("skills");
let skill = [
    {judul: "HTML 5", icon: '<i class="fab fa-html5"></i>'},
    {judul: "CSS", icon: '<i class="fab fa-css3-alt"></i>'},
    {judul: "JavaScript", icon: '<i class="fab fa-js-square"></i>'},
    {judul: "PHP", icon: '<i class="fab fa-php"></i>'}
];
$.each(skill, function(i){
    let templateString = 
    `
    <div class="col-md-3 section-1-box wow fadeInUp">
        <div class="row">
            <div class="col-md-4">
                <div class="section-1-box-icon">
                    `+skill[i].icon+`
                </div>
            </div>
            <div class="col-md-8">
                <h3>`+skill[i].judul+`</h3>
            </div>
        </div>
    </div>
    `;
    skills.innerHTML += templateString;
})

// content about
let about = document.getElementById("par");
about.innerHTML = `I am an adaptable person and have a desire to keep learning. If I am accepted into your company,
I will work as well as you want. Even though I just graduated, but I have some experience.<br>
For three months I had an internship at a digital company and there I helped the IT division
staff and I also made software for the company. Apart from my internship, I was also a assistant lecturer in
a software development course while I was still in college. I am now looking for a new challenge that will
provide me the opportunity to further develop my software development career.`;

// content education
let education = document.getElementById("education");
let edu = [
    {institute: "Sanata Dharma University", title:"Bachelor of Computer", program:"Informatics Study Program", date:"August 2016 - July 2020"},
    {institute: "State High School 7 Yogyakarta", title:"", program:"Science Program", date:"July 2013 - June 2016"}
];
$.each(edu, function(i){
    let templateString = 
    `
    <div class="col-md-12 section-3-box wow fadeInLeft">
        <div class="row d-flex justify-content-center mb-2 section-3-border">
            <div class="col-sm-6 d-flex flex-column flex-md-row justify-content-between mb-5">
                <div class="flex-grow-1">
                    <h3 class="mb-0 mt-3">`+edu[i].institute+`</h3>
                        <p>`+edu[i].title+`</p>
                        <p>`+edu[i].program+`</p>
                </div>
            </div>
            <div class="col-sm-4 d-flex flex-column flex-md-row justify-content-between mb-5 mt-3">
                <div class="flex-shrink-0"><span class="text-dark">`+edu[i].date+`</span></div>
            </div>
        </div>
    </div>
    `;
    education.innerHTML += templateString;
});

// content portfolio
let portfolio = document.getElementById("porto");
let porto = [
    {judul: "School Website & Academic Information System", type : "Freelance Project", desc : "For Kanisius Sengkan Elementary School", link : "https://drive.google.com/file/d/15Vx0FjoY0Pzb3vXa5AIcyRl9eOiE2KPd/view?usp=sharing"},
    {judul: "Order Filtering System", type : "Internship Project", desc : "For PT Yoofix Digital Indonesia", link : "https://drive.google.com/file/d/1LUi9f8BwRNPvfblDThRIfxZNOXuB3Iit/view?usp=sharing"},
    {judul: "Consultation Scheduling System", type : "Course Assignments Project", desc : "This is final project for software engineering courses, Informatics Study Program, Sanata Dharma University", link : "https://drive.google.com/file/d/1t3uczeYppIzUtgx-rn8oNJmN3NKtGyze/view?usp=sharing"}
];
$.each(porto, function(i){
    let templateString = 
    `
    <div class="col-md-4 d-flex section-5-box wow">
        <div class="card rounded" >
            <div class="card-body d-flex flex-column h-100">
                <h5 class="card-title">`+porto[i].judul+`</h5>
                <p class="card-text">Type : `+porto[i].type+`</p>
                <p class="card-text">`+porto[i].desc+`</p>
                <a href="`+porto[i].link+`" target="_blank" class="mt-auto btn">View More</a>
            </div>
        </div>
    </div>
    `;
    portfolio.innerHTML += templateString;
});

// content contact
let contact = document.getElementById("contact");
let ct = [
    {icon: '<i class="fab fa-telegram-plane"></i>', title:"Telegram", desc: "@andreasyanpratama", link: "https://t.me/andreasyanpratama"},
    {icon: '<i class="far fa-envelope"></i>', title:"GMAIL", desc: "andreasyanpratama@gmail.com", link: "mailto:andreasyanpratama@gmail.com"},
    {icon: '<i class="fab fa-github"></i>', title:"Github", desc: "AndreasYanPratama", link: "https://github.com/AndreasYanPratama"},
    {icon: '<i class="fab fa-gitlab"></i>', title:"Gitlab", desc: "AndreasYanPratama", link: "https://gitlab.com/AndreasYanPratama"}
];
$.each(ct, function(i){
    let templateString = 
    `
    <div class="row">
        <a href="`+ct[i].link+`" title="`+ct[i].title+`" target="_blank">`+ct[i].icon+`</a>
        <span>&nbsp;`+ct[i].desc+`</span>
    </div>
    `;
    contact.innerHTML += templateString;
});

function scroll_to(clicked_link, nav_height) {
    var element_class = clicked_link.attr('href').replace('#', '.');
    var scroll_to = 0;
    if(element_class != '.top-content') {
        element_class += '-container';
        scroll_to = $(element_class).offset().top - nav_height;
    }
    if($(window).scrollTop() != scroll_to) {
        $('html, body').stop().animate({scrollTop: scroll_to}, 1000);
    }
}

// code jquery
jQuery(document).ready(function() {
    // menutup sidebar dan membatalkan efek overlay
    $('.dismiss, .overlay').on('click', function() {
        $('.sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });
 
    // klik tombol menu sidebar dan menambahkan efek overlay
    $('.open-menu').on('click', function(e) {
        e.preventDefault();
        $('.sidebar').addClass('active');
        $('.overlay').addClass('active');
        // close opened sub-menus
        $('.collapse.show').toggleClass('show');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });

    /* replace the default browser scrollbar in the sidebar, in case the sidebar menu has a height that is bigger than the viewport */
	$('.sidebar').mCustomScrollbar({
		theme: "minimal-dark"
	});

    /*
    Navigation
    */
   // membuat animasi scroll
    $('a.scroll-link').on('click', function(e) {
        e.preventDefault();
        scroll_to($(this), 0);
    });
    
    // membuat tombol kembali ke atas
    $('.to-top a').on('click', function(e) {
        e.preventDefault();
        if($(window).scrollTop() != 0) {
            $('html, body').stop().animate({scrollTop: 0}, 1000);
        }
    });

    /* make the active menu item change color as the page scrolls up and down */
	/* we add 2 waypoints for each direction "up/down" with different offsets, because the "up" direction doesn't work with only one waypoint */
    $('.section-container').waypoint(function(direction) {
        if (direction === 'down') {
            $('.menu-elements li').removeClass('active');
            $('.menu-elements a[href="#' + this.element.id + '"]').parents('li').addClass('active');
        }
    },
    {
        offset: '0'
    });
     
    $('.section-container').waypoint(function(direction) {
        if (direction === 'up') {
            $('.menu-elements li').removeClass('active');
            $('.menu-elements a[href="#' + this.element.id + '"]').parents('li').addClass('active');
        }
    },
    {
        offset: '-5'
    });

    // membuat animasi ketikan text dg teletype
    $(function() {
        $('.type-text').each(function() {
            var items = $(this).attr('title') + ';' + $(this).text();
            $(this).empty().attr('title', '').teletype({
                text: $.map(items.split(';'), $.trim),
                typeDelay: 10,
                backDelay: 20,
                cursor: '▋',
                delay: 3000,
                preserve: false,
                prefix: '[teletype ~]# ',
                loop: 0
            });
        });
    });

    // memvalidasi form di section contact me dg jqueryvalidate
    $("form[name='pengiriman']").validate({
        rules: {
            subject: "required",
            message: "required",
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            subject: "Please enter a subject",
            message: "Please enter a message",
            email: "Please enter a valid email address"
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
   
});